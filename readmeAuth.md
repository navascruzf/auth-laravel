## Install

composer install <br>
php artisan migrate <br>
php artisan key:generate <br>
php artisan passport:install <br>


UrlBase: localhost:8000/api

##Endpoints

 - auth/signup
    Method: POST
    Body: {
        "name"
        "email"
        "password"
        "password_confirmation"
    }
    Response: {
        "message": "Successfully created user!"
    }

 - auth/login
    Method: POST
    Body: {
        "email"
        "password"
        "remember_me": boolean
    }
    Response: {
        "access_token":
        "token_type": "Bearer",
        "expires_at":
    }

 - auth/logout
    Method: GET
    Headers: {
        "Authorization": "Bearer `Access Token`"
    }
    Response: {
        "message": "Successfully logged out"
    }

- auth/user
    Method: GET
    Headers: {
        "Authorization": "Bearer `Access Token`"
    }
    Response: {
        "id"
        "name"
        "email"
        "avatar"
        "active"
        "created_at"
        "updated_at"
        "deleted_at"
        "avatar_url"
    }

- auth/signup/activate/`token enviado por correo`   

    // despues de resgistrarse le llegara un correo con un link a este endpoint

    Response: {
        "id"
        "name"
        "avatar"
        "active"
        "created_at"
        "updated_at"
        "deleted_at"
    }

- password/create
    Method: POST
    Body: {
        "email"
    }
    Response: {
        "message": "We have e-mailed your password reset link!"
    }

- password/find/`token`   

    // este link sera enviado al correo para resetear el password
    
    Method: GET
    Response: {
        "id"
        "email"
        "token"
        "created_at"
        "updated_at"
    }

    or
    {
        "message": "This password reset token is invalid."
    }

- password/reset

    Method: POST
    Body: {
        "email"
        "password"
        "password_confirmation"
        "token" `token del endpoint anterior`
    }
    Response: {
        "id"
        "name"
        "email"
        "avatar"
        "active"
        "created_at"
        "updated_at"
        "deleted_at"
        "avatar_url"
    }
    Si el token no es valido la respuesta sera 
    {
        "message": "This password reset token is invalid."
    }
